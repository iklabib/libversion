import { expect, test } from "bun:test";
import { versionCompare2, versionCompare4, VersionFlag } from "./compare";

test("Comparison: 0.99 < 1.11", () => {
    expect(versionCompare2("0.99", "1.11")).toBe(-1);
});

test("Comparison: 1.0 == 1.0.0", () => {
    expect(versionCompare2("1.0", "1.0.0")).toBe(0);
});

test("Comparison: 1.0alpha1 < 1.0.rc1", () => {
    expect(versionCompare2("1.0alpha1", "1.0.rc1")).toBe(-1);
});

test("Comparison: 1.0 > 1.0.rc1", () => {
    expect(versionCompare2("1.0", "1.0-rc1")).toBe(1);
});

test("Comparison: 1.2.3alpha4 is the same as 1.2.3~a4", () => {
    expect(versionCompare2("1.2.3alpha4", "1.2.3~a4")).toBe(0);
});

test("Default handling: 'p' is treated as 'pre'", () => {
    expect(versionCompare2("1.0p1", "1.0pre1")).toBe(0);
    expect(versionCompare2("1.0p1", "1.0post1")).toBe(-1);
    expect(versionCompare2("1.0p1", "1.0patch1")).toBe(-1);
});

test("Tunable handling: 'p' is handled as 'patch'", () => {
    expect(versionCompare4("1.0p1", "1.0pre1", VersionFlag.P_IS_PATCH, 0)).toBe(1);
    expect(versionCompare4("1.0p1", "1.0post1", VersionFlag.P_IS_PATCH, 0)).toBe(0);
    expect(versionCompare4("1.0p1", "1.0patch1", VersionFlag.P_IS_PATCH, 0)).toBe(0);
});

test("Check versions belong to a given release", () => {
    expect(versionCompare4("1.0alpha1", "1.0", 0, VersionFlag.LOWER_BOUND)).toBe(1);
    expect(versionCompare4("1.0alpha1", "1.0", 0, VersionFlag.UPPER_BOUND)).toBe(-1);
    expect(versionCompare4("1.0.1", "1.0", 0, VersionFlag.LOWER_BOUND)).toBe(1);
    expect(versionCompare4("1.0.1", "1.0", 0, VersionFlag.UPPER_BOUND)).toBe(-1);
});
