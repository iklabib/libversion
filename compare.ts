import { dlopen, FFIType, suffix } from "bun:ffi";

enum VersionFlag {
    P_IS_PATCH = 0x1,
    ANY_IS_PATCH = 0x2,
    LOWER_BOUND = 0x4,
    UPPER_BOUND = 0x8,
}

const symbols = {
    version_compare2: {
        args: [FFIType.cstring, FFIType.cstring],
        returns: FFIType.int,
    },
    version_compare4: {
        args: [FFIType.cstring, FFIType.cstring, FFIType.int, FFIType.int],
        returns: FFIType.int,
    }
}

const lib = dlopen(`./lib/libversion.${suffix}`, symbols);
const { symbols: { version_compare2, version_compare4 } } = lib

const versionCompare2 = (v1: string, v2: string) => {
    const _v1 = Buffer.from(v1, 'utf8');
    const _v2 = Buffer.from(v2, 'utf8');
    return version_compare2(_v1, _v2);
}

const versionCompare4 = (v1: string, v2: string, v1_flag: VersionFlag | 0, v2_flag: VersionFlag | 0) => {
    const _v1 = Buffer.from(v1, 'utf8');
    const _v2 = Buffer.from(v2, 'utf8');
    return version_compare4(_v1, _v2, v1_flag, v2_flag);
}

export {
    versionCompare2,
    versionCompare4,
    VersionFlag
}